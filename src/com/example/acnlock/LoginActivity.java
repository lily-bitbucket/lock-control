package com.example.acnlock;

import com.example.acnlock.R;
import com.example.acnlock.services.AccountService;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;

public class LoginActivity extends Activity {
	Button login;
	Button cancel;
	EditText username;
	EditText password;
	EditText show;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		login = (Button) findViewById(R.id.btn_login);
		cancel = (Button) findViewById(R.id.btn_cancel);
		username = (EditText) findViewById(R.id.et_un);
		password = (EditText) findViewById(R.id.et_pw);
		// show = (EditText) findViewById(R.id.show);

		login.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				String un = username.getText().toString();
				String pw = password.getText().toString();
				try {
					if (AccountService.login(un, pw)) {
						// if verification is true, intent to the new
						// activity;else, return the current activity
						Intent intent = new Intent(LoginActivity.this,
								LockActivity.class);
						startActivityForResult(intent, 0);
						finish();
					} else {
						Toast.makeText(getBaseContext(), "Login again!",
								Toast.LENGTH_LONG).show();
						username.setText("");
						password.setText("");
					}

				} catch (Exception e) {
					show.setText("fail");
					e.printStackTrace();
				}
			}

		});
	}

	// useless code below
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

}

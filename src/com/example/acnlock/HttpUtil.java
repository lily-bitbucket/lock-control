package com.example.acnlock;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.os.StrictMode;

public class HttpUtil {

	public static HttpClient httpclient = new DefaultHttpClient();

	public static String getRequestStringAndSucc(String token, String url) throws Exception {
		HttpResponse response = getRequestResponse(token, url);
		int status = response.getStatusLine().getStatusCode();
		if (status == 200) {
			String result = EntityUtils.toString(response.getEntity());
			return result;
		}
		return null;
	}

	public static int getRequestStatus(String token, String url)
			throws Exception {
		HttpResponse response = getRequestResponse(token, url);
		return response.getStatusLine().getStatusCode();
	}

	private static HttpResponse getRequestResponse(String token, String url) throws Exception {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		HttpGet get = new HttpGet(url);
		get.setHeader("token", token);
		HttpResponse response = httpclient.execute(get);
		return response;
	}

	public static JSONObject postRequest(String url, Map<String, String> params)
			throws Exception {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		HttpPost post = new HttpPost(url);
		JSONObject jParams = new JSONObject(params);
		StringEntity input = new StringEntity(jParams.toString());
		input.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
				"application/json"));
		post.setHeader("Content-type", "application/json");
		post.setEntity(input);

		HttpResponse response = httpclient.execute(post);

		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed: HTTP error code:"
					+ response.getStatusLine().getStatusCode());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(
				(response.getEntity().getContent())));
		StringBuilder output = new StringBuilder();
		String t = null;
		while ((t = br.readLine()) != null) {
			output.append(t);
		}
		JSONObject jOutput = new JSONObject(output.toString());
		httpclient.getConnectionManager().shutdown();
		return jOutput;

	}
}
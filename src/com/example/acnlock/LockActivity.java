package com.example.acnlock;

import com.example.acnlock.R;
import com.example.acnlock.services.AccountService;
import com.example.acnlock.services.LockService;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class LockActivity extends Activity {
	Button logout;
	Button unlock;
	Button lock;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.unlock);
		logout = (Button) findViewById(R.id.btn_logout);
		unlock = (Button) findViewById(R.id.btn_unlock);
		
		logout.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				if (!AccountService.logout()) {
					Toast.makeText(getBaseContext(), "Logout again!",
							Toast.LENGTH_LONG).show();
				}
			}
		});
		
		unlock.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				if(LockService.unlock(AccountService.getCurrentToken()).equals("true")){
					Toast.makeText(getBaseContext(), "Unlock succeed!",
							Toast.LENGTH_LONG).show();
				}
				else {
					Toast.makeText(getBaseContext(), "Unlock failed",
							Toast.LENGTH_LONG).show();
				}
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.unlock, menu);
		return true;
	}

}

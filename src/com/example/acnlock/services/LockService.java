package com.example.acnlock.services;

import com.example.acnlock.HttpUtil;

public class LockService {
	public static final String LOCKSTATUS_URL = "http://10.0.2.2:8080/RestfulHelloWorld/rest/lockstatus";
	public static final String UNLOCK_URL = "http://10.0.2.2:8080/RestfulHelloWorld/rest/unlock";
	
	public static String checkStatus(String token) {
		try {
			return HttpUtil.getRequestStringAndSucc(token, LOCKSTATUS_URL);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String unlock(String token) {
		try {
			return HttpUtil.getRequestStringAndSucc(token, UNLOCK_URL);
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
			
}

package com.example.acnlock.services;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.widget.Toast;

import com.example.acnlock.HttpUtil;

public class AccountService {
	public static final String LOGIN_URL = "http://10.0.2.2:8080/RestfulHelloWorld/rest/login";
	public static final String LOGOUT_URL = "http://10.0.2.2:8080/RestfulHelloWorld/rest/logout";

	private static String currentToken = null;

	public static boolean login(String un, String pw) {
		JSONObject jsonObj;
		try {
			jsonObj = query(un, pw);
			String token = jsonObj.getString("token");
			if (token != null) {
				currentToken = token;
				return true;
			}
		} catch (Exception e) {
			Toast.makeText(null, "there is an exception.", Toast.LENGTH_LONG)
					.show();
			e.printStackTrace();
		}
		return false;
	}

	public static String getCurrentToken() {
		return currentToken;
	}

	private static JSONObject query(String un, String pw) throws JSONException,
			Exception {
		Map<String, String> map = new HashMap<String, String>();
		map.put("user", un);
		map.put("pass", pw);
		return HttpUtil.postRequest(LOGIN_URL, map);
	}

	public static boolean logout() {
		if (currentToken == null)
			return true;

		try {
			return 200 == HttpUtil.getRequestStatus(currentToken, LOGIN_URL);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
